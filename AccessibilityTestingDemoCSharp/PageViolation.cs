﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessibilityTestingDemoCSharp
{
    public class PageViolation
    {
        internal string pageTitle;
        internal string pageURL;
        int violationCount;
        List<RuleViolation> violationsList = new List<RuleViolation>();
        
		internal string pageFolderName;

        internal string passedRules;

        /// <summary>
        /// Gets the passed rules.
        /// </summary>
        /// <returns> the passed rules </returns>
        public virtual string PassedRules
        {
            get
            {
                return passedRules;
            }
            set
            {
                this.passedRules = value;
            }
        }


        /// <summary>
        /// Gets the page folder name.
        /// </summary>
        /// <returns> the pageFolderName </returns>
        public virtual string PageFolderName
        {
            get
            {
                return pageFolderName;
            }
            set
            {
                this.pageFolderName = value.Replace(" ", "_");
            }
        }


        /// <summary>
        /// Gets the violations list.
        /// </summary>
        /// <returns> the violationsList </returns>
        public virtual List<RuleViolation> ViolationsList
        {
            get
            {
                return violationsList;
            }
            set
            {
                this.violationsList = value;
            }
        }


        /// <summary>
        /// Gets the page title.
        /// </summary>
        /// <returns> the pageTitle </returns>
        public virtual string PageTitle
        {
            get
            {
                return pageTitle;
            }
            set
            {
                this.pageTitle = value.Replace(" ", "_");
            }
        }


        /// <summary>
        /// Gets the page url.
        /// </summary>
        /// <returns> the pageUrl </returns>
        public virtual string PageUrl
        {
            get
            {
                return pageURL;
            }
            set
            {
                this.pageURL = value;
            }
        }


        /// <summary>
        /// Gets the violation count.
        /// </summary>
        /// <returns> the violationCount </returns>
        public virtual int ViolationCount
        {
            get
            {
                return violationCount;
            }
            set
            {
                this.violationCount = value;
            }
        }

    }

}

