using OpenQA.Selenium;
using Selenium.Axe;
using OpenQA.Selenium.Chrome;
using CSharpAssignment_Oct17.Helpers;
using System.Configuration;
using System;
using System.IO;
using OpenQA.Selenium.Firefox;
using AccessibilityTestingDemoCSharp;
using System.Collections.Generic;
using AccessibilityTestingDemoCSharp.Rules;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AccessibilityTestingDemoCSharp
{
    [TestClass]
    public class BaseTests
    {
        
        public IWebDriver driver;
        AccessibilityCOP cop;


        [TestInitialize]
        public void OneTimeSetup()
        {
            driver = new FirefoxDriver();
            driver.Manage().Window.Maximize();
            cop = new AccessibilityCOP(driver);
            

        }

        [TestMethod]
        public void PerformAccessbilityTestOne()
        {
            driver.Navigate().GoToUrl("https://www.google.com");
            cop.generateAccessibilityReportForCurrentPage("High");
            Thread.Sleep(2000);
        }
            [TestMethod]
            public void Test()
        {
            driver.Navigate().GoToUrl("www.google.com");
        }
            

        [TestCleanup]
        public void OneTimeTearDown()
        {
            cop.generateOverallAccessibilityReport();
            driver.Quit();
            driver.Close();
        }
    }
}