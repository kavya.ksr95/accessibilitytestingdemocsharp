﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessibilityTestingDemoCSharp.Rules
{
    class ButtonRule : IAccessibilityRule
    {
        public string Description => throw new NotImplementedException();
        public IEnumerable<AccessibilityViolation> Check(IWebDriver driver)
        {
            return driver.FindElements(By.TagName("button"))
                .Concat(driver.FindElements(By.CssSelector("[role='button']")))
                .Concat(driver.FindElements(By.CssSelector("input[type='button']")))
                .Concat(driver.FindElements(By.CssSelector("input[type='submit']")))
                .Concat(driver.FindElements(By.CssSelector("input[type='reset']")))
                .Where(x => x.GetAttribute("aria-label") == "")
                .Select(x => new AccessibilityViolation()
                {
                    Element = x,
                    Violation = Description
                });
        }
    }
}
