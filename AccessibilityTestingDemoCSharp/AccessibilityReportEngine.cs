﻿using NVelocity;
using NVelocity.App;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessibilityTestingDemoCSharp
{
    public class AccessibilityReportEngine
    {
        private static readonly string reportFolder = "AccessibilityReports/";
        public void ReportAccessibilityViolations(PageViolation pageViolation)
        {
            string pageTitle = pageViolation.pageTitle;
            string URL = pageViolation.PageUrl;

            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string pathTillBin = path.Substring(0, path.LastIndexOf("bin"));
            string projectPath = new Uri(pathTillBin).LocalPath;

            if (!Directory.Exists(projectPath))
            {
                Directory.Exists(projectPath);
            }

            generateRuleViolationsInDetail(pageViolation.ViolationsList, URL, pageViolation.PageFolderName);
        }
        /// <summary>
        /// Generates rule violations in detail.
        /// </summary>
        /// <param name="violationsList"></param>
        /// <param name="URL"></param>
        /// <param name="title"></param>
        private void generateRuleViolationsInDetail(List<RuleViolation> violationsList, string URL, string title)
        {
            try
            {
                foreach (RuleViolation ruleViolation in violationsList)
                {
                    List<Dictionary<String, string>> rulesList = new List<Dictionary<String, String>>();
                    VelocityEngine velocityEngine = new VelocityEngine();
                    velocityEngine.Init();

                    Template template = velocityEngine.GetTemplate("accessibility_report_rule_violation_template.vm");

                    VelocityContext velocityContext = new VelocityContext();

                    // put the states and the transitions into the context
                    velocityContext.Put("Title", title);
                    velocityContext.Put("Url", URL);
                    velocityContext.Put("Date", new DateTime().ToString());
                    velocityContext.Put("Rule", ruleViolation.Rule);
                    velocityContext.Put("Description", ruleViolation.Description);


                    for (int i = 0; i < ruleViolation.ViolationList.Count(); i++)
                    {
                        Dictionary<String, String> temp = new Dictionary<String, String>();
                        temp.Add("violation", ruleViolation.ViolationList.ElementAt(i));
                        if (ruleViolation.getViolationMessage().Count() > 0)
                        {
                            temp.Add("message", ruleViolation.getViolationMessage().ElementAt(i));
                        }
                        else
                        {
                            temp.Add("message", "");
                        }
                        if (ruleViolation.getViolationTarget().Count() > 0)
                        {
                            temp.Add("target", ruleViolation.getViolationTarget().ElementAt(i));
                        }
                        else
                        {
                            temp.Add("target", "");
                        }

                        rulesList.Add(temp);
                    }
                    velocityContext.Put("RulesList", rulesList);


                    if (Directory.Exists(reportFolder + title))
                    {
                        Directory.Delete(reportFolder);
                    }


                    StringWriter writer = new StringWriter();
                    template.Merge(velocityContext, writer);

                    File.WriteAllText((reportFolder + title + "/" + ruleViolation.Id + "_Violation_Report.html"), writer.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// Generates overall accessibility reports using velocity engine
        /// </summary>
        public void generateOverallAccessibilityReports()
        {
            try
            {
                VelocityEngine velocityEngine1 = new VelocityEngine();
                velocityEngine1.Init();

                Template template1 = velocityEngine1.GetTemplate("accessibility_report_page_overall_summary_template.vm");
                VelocityContext velocityContext1 = new VelocityContext();

                velocityContext1.Put("Passed", ReportData.passed);
                velocityContext1.Put("Failed", ReportData.failed);
                velocityContext1.Put("TotalCount", (ReportData.overallPassedRules + ReportData.totalRuleViolations));
                velocityContext1.Put("RuleViolations", ReportData.totalRuleViolations);
                velocityContext1.Put("OverallPassed", ReportData.overallPassedRules);
                velocityContext1.Put("Date", new DateTime().ToString());
                velocityContext1.Put("TotalPages", ReportData.totalPages);
                velocityContext1.Put("PassPercentage", ((ReportData.overallPassedRules * 100)
                        / (ReportData.overallPassedRules + ReportData.totalRuleViolations)));
                velocityContext1.Put("FailurePercentage", ((ReportData.totalRuleViolations * 100)
                        / (ReportData.overallPassedRules + ReportData.totalRuleViolations)));

                List<Dictionary<String, String>> violations = new List<Dictionary<String, String>>();
                List<PageViolation> pagesViolation = ReportData.violationAnalysis;

                foreach (PageViolation pageViolations in pagesViolation)
                {
                    Dictionary<string, string> tempMap = new Dictionary<string, string>();
                    tempMap.Add("PageTitle", pageViolations.PageTitle);
                    tempMap.Add("PageURL", pageViolations.PageUrl);
                    tempMap.Add("RuleViolations", (pageViolations.ViolationsList.Count().ToString()));
                    tempMap.Add("TotalViolations", (pageViolations.ViolationCount).ToString());
                    tempMap.Add("PageSummary",
                            "AccessibilityReports\\" + pageViolations.pageFolderName + "_Summary.html");
                    tempMap.Add("PassedRules", pageViolations.PassedRules);

                    violations.Add(tempMap);
                }
                velocityContext1.Put("violations", violations);
                StringWriter writer1 = new StringWriter();
                template1.Merge(velocityContext1, writer1);

                File.WriteAllText(("Overall_Summary.html"), writer1.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void generateOverallAccessibilityReport()
        {
            AccessibilityReportEngine reportEngine = new AccessibilityReportEngine();
            reportEngine.generateOverallAccessibilityReports();
        }
    }
}
