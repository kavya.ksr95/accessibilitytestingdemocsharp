﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessibilityTestingDemoCSharp
{
    public class ReportData
    {
        
		public static int totalPages = 0;

        
        public static int passed = 0;

        
        public static int failed = 0;

        
        public static int totalFlaws = 0;

        
        public static int totalRuleViolations = 0;

        
        public static List<PageViolation> violationAnalysis = new List<PageViolation>();

        
        public static int overallPassedRules = 0;
    }
}
