﻿using OpenQA.Selenium;
using Selenium.Axe;
using System;
using System.Collections.Generic;
using System.Text;

namespace AccessibilityTestingDemoCSharp
{
    public class AccessibilityCOP
    {
        private IWebDriver driver;
        public AccessibilityCOP(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void generateAccessibilityReportForCurrentPage(string title)
        {
            PageViolation pageViolation = new PageViolation();
            List<RuleViolation> ruleViolationsList = new List<RuleViolation>();

            AxeResult results = driver.Analyze();

            foreach (var message in results.Violations)
            {
                pageViolation.ViolationCount = results.Violations.Length;
                pageViolation.PageTitle = driver.Title;
                pageViolation.PageUrl = results.Url;
                pageViolation.PassedRules = results.Passes.Length.ToString();
            }

            if (results.Violations.Length == 0)
            {
                ReportData.passed = ReportData.passed + 1;
            }
            else
            {
                ReportData.failed++;
            }
            ReportData.overallPassedRules = ReportData.overallPassedRules + results.Passes.Length;
            ReportData.totalRuleViolations = ReportData.totalRuleViolations + results.Violations.Length;

            //Rule Violation
            RuleViolation ruleViolation = new RuleViolation();

            String desc = ruleViolation.Description.ToString();

            if (desc.Contains("<") && desc.Contains(">"))
            {
                desc = desc.Replace("<", "&lt;");
                desc = desc.Replace(">", "&gt;");
                ruleViolation.Description = desc;
            }
            else
            {
                ruleViolation.Description = desc;
            }
            foreach (var message in results.Violations)
            {
                ruleViolation.HelpURL = message.HelpUrl;
                ruleViolation.Impact = message.Impact;
                ruleViolation.Id = message.Id;
                string tags = "";
                for (int i = 0; i < message.Tags.Length; i++)
                {
                    ruleViolation.Rule = tags;
                    tags += message.Tags.GetValue(i) + "/n";
                }
                for (int j = 0; j < message.Nodes.Length; j++)
                {
                    ruleViolation.Violation = message.Nodes.GetValue(j).GetType().ToString();

                }

                ruleViolationsList.Add(ruleViolation);
            }
            pageViolation.ViolationsList = ruleViolationsList;
            String pageFolderName = pageViolation.PageTitle;

            // Handle special characters for Page title

            pageFolderName = pageFolderName.Replace("\\|", "");
            pageFolderName = pageFolderName.Replace("&", "");
            pageFolderName = pageFolderName.Replace("$", "");
            pageFolderName = pageFolderName.Replace("%", "");
            pageFolderName = pageFolderName.Replace("@", "");
            pageFolderName = pageFolderName.Replace("'", "");
            pageFolderName = pageFolderName.Replace("™", "");

            pageViolation.PageFolderName = pageFolderName;
            ReportData.violationAnalysis.Add(pageViolation);

            AccessibilityReportEngine reportEngine = new AccessibilityReportEngine();
            reportEngine.ReportAccessibilityViolations(pageViolation);

            //This Generates the overall report using NVelocity
            generateOverallAccessibilityReport();


        }

        public void generateOverallAccessibilityReport()
        {
            AccessibilityReportEngine reportEngine = new AccessibilityReportEngine();
            reportEngine.generateOverallAccessibilityReports();
        }
    }
}
