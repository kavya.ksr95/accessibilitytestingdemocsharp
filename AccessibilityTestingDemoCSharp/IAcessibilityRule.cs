﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessibilityTestingDemoCSharp
{
    public interface IAccessibilityRule
    {
        IEnumerable<AccessibilityViolation> Check(IWebDriver driver);
        string Description { get; }
    }

    public class AccessibilityViolation
    {
        public IWebElement Element { get; set; }
        public string Violation;
    }
}
