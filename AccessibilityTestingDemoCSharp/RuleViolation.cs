﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessibilityTestingDemoCSharp
{
    public class RuleViolation
    {
        internal string Id { get; set; }

        public virtual string HelpURL { get; set; }
        public virtual string Impact { get; set; }
        public virtual string Target { get; set; }
        public virtual string Samples { get; set; }
        public virtual string Description { get; set; }
        public virtual string Rule { get; set; }
        
        


            /// <summary>
            /// The violation list. </summary>
            internal IList<string> violationList = new List<string>();

            /// <summary>
            /// The violation message. </summary>
            internal IList<string> violationMessage = new List<string>();

            /// <summary>
            /// The violation target. </summary>
            internal IList<string> violationTarget = new List<string>();




            /// <summary>
            /// Gets the violation list.
            /// </summary>
            /// <returns> the violationList </returns>
            public virtual IList<string> ViolationList
            {
                get
                {
                    return violationList;
                }
            }

            /// <summary>
            /// Gets the violation message.
            /// </summary>
            /// <returns> the violationMessage </returns>
            public virtual IList<string> getViolationMessage()
            {
                return violationMessage;
            }

            /// <summary>
            /// Gets the violation target.
            /// </summary>
            /// <returns> the violationTarget </returns>
            public virtual IList<string> getViolationTarget()
            {
                return violationTarget;
            }

            /// <summary>
            /// Sets the violation.
            /// </summary>
            /// <param name="violation"> the new violation </param>
            public virtual string Violation
            {
                set
                {
                    this.violationList.Add(value);
                }
            }

            /// <summary>
            /// Sets the violation message.
            /// </summary>
            /// <param name="violationMessage"> the new violation message </param>
            public virtual void SetViolationMessage(string violationMessage)
            {
                this.violationMessage.Add(violationMessage);
            }

            /// <summary>
            /// Sets the violation target.
            /// </summary>
            /// <param name="violationTarget"> the new violation target </param>
            public virtual void SetViolationTarget(string violationTarget)
            {
                this.violationTarget.Add(violationTarget);
            }
        }

    }

